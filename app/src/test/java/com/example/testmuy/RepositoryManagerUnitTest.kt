package com.example.testmuy

import androidx.lifecycle.MutableLiveData
import com.example.testmuy.base.TestLifecycleOwner
import com.example.testmuy.mocks.getCompanyResponse
import com.example.testmuy.mocks.getEmployeeList
import com.example.testmuy.model.Employee
import com.example.testmuy.repository.EmployeeLocalDataSource
import com.example.testmuy.repository.EmployeeRemoteDataSource
import com.example.testmuy.repository.RepositoryManager
import com.example.testmuy.viewmodel.EmployeesViewModel
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner
import org.powermock.api.mockito.PowerMockito
import org.powermock.modules.junit4.PowerMockRunner
import org.powermock.modules.junit4.PowerMockRunnerDelegate


@RunWith(PowerMockRunner::class)
@PowerMockRunnerDelegate(MockitoJUnitRunner::class)
class RepositoryManagerUnitTest {

    private lateinit var mockRepositoryManager: RepositoryManager
    private lateinit var mockRemoteDataSource: EmployeeRemoteDataSource
    private lateinit var mockLocalDataSource: EmployeeLocalDataSource
    private lateinit var mockEmployeesViewModel: EmployeesViewModel
    private val testLifecycleOwner = TestLifecycleOwner()
    private lateinit var mockEmployees : MutableLiveData<ArrayList<Employee>>
    private lateinit var mockIsSorted : MutableLiveData<Boolean>
    private lateinit var mockAllSaved : MutableLiveData<Boolean>

    @Before
    fun setUp() {
        mockRemoteDataSource = PowerMockito.mock(EmployeeRemoteDataSource::class.java)
        mockLocalDataSource = PowerMockito.mock(EmployeeLocalDataSource::class.java)
        mockRepositoryManager = RepositoryManager(mockRemoteDataSource, mockLocalDataSource)
        mockEmployees = MutableLiveData()
        mockIsSorted = MutableLiveData()
        mockAllSaved = MutableLiveData()
    }

    @Test
    fun testRepositoryManager(){
        mockEmployeesViewModel = EmployeesViewModel(mockRepositoryManager)

        // Start LifecycleOwner
        startLifeCycle()

        //Given
        PowerMockito.`when`(mockRemoteDataSource.getAllEmployees()).thenReturn(getCompanyResponse())
        PowerMockito.`when`(mockEmployeesViewModel.getEmployeesLiveData()).thenReturn(mockEmployees)

    }

    private fun startLifeCycle() {
        // Start LifecycleOwner
        testLifecycleOwner.onCreate()
        testLifecycleOwner.onStart()
    }
}