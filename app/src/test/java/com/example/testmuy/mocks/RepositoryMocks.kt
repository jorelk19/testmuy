package com.example.testmuy.mocks

import com.example.testmuy.model.CompanyResponse
import com.example.testmuy.model.Employee

fun getEmployeeList() : ArrayList<Employee>{
    val list = arrayListOf<Employee>()
    list.add(Employee(id=1, name = "Edson 1", position = "Developer", wage = 12345.0))
    list.add(Employee(id=2, name = "Edson 2", position = "Developer", wage = 12345.0))
    list.add(Employee(id=3, name = "Edson 3", position = "Developer", wage = 12345.0))
    return  list
}


fun getCompanyResponse() : CompanyResponse{
    return CompanyResponse(
        companyName = "Test",
        address = "calle 123",
        employees = getEmployeeList()
    )
}
