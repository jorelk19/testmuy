package com.example.testmuy.view.employee

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.testmuy.R
import com.example.testmuy.databinding.EmployeeDetailFragmentBinding
import com.example.testmuy.di.ApplicationManager
import com.example.testmuy.model.Employee
import com.example.testmuy.view.employee.adapters.EmployeeDetailAdapter
import java.text.NumberFormat
import java.util.*

class EmployeeDetailFragment(private val applicationManager: ApplicationManager) : Fragment() {
    private lateinit var currentEmployee: Employee
    private lateinit var employeeDetailFragmentBinding : EmployeeDetailFragmentBinding
    private var employeeDetailAdapter: EmployeeDetailAdapter? = null

    companion object {
        fun newInstance(employee: Employee, applicationManager: ApplicationManager): EmployeeDetailFragment {
            val employeeDetailFragment = EmployeeDetailFragment(applicationManager)
            employeeDetailFragment.setCurrentEmployee(employee)
            return employeeDetailFragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        employeeDetailFragmentBinding = DataBindingUtil.inflate(inflater, R.layout.employee_detail_fragment, container, false)
        buildAdapter()
        setLayoutManager()
        setAdapters()
        loadDetail()
        return employeeDetailFragmentBinding.root
    }

    private fun loadDetail() {
        employeeDetailFragmentBinding.chkIsNewEmployee.isChecked = currentEmployee.isNewEmployee
        employeeDetailFragmentBinding.tvEmployeeName.text = currentEmployee.name
        employeeDetailFragmentBinding.tvEmployeePosition.text = currentEmployee.position
        employeeDetailFragmentBinding.tvEmployeeWage.text = NumberFormat.getCurrencyInstance(Locale.US).format(currentEmployee.wage).substringBefore(".")
        employeeDetailAdapter?.let {
            it.setEmployees(currentEmployee.employees)
        }
    }

    private fun setCurrentEmployee(employee: Employee){
        currentEmployee = employee
    }

    private fun setLayoutManager() {
        employeeDetailFragmentBinding.rvReportedEmployees.layoutManager = LinearLayoutManager(applicationManager.currentContext)
    }

    private fun buildAdapter(){
        employeeDetailAdapter = EmployeeDetailAdapter()
    }

    private fun setAdapters() {
        employeeDetailFragmentBinding.rvReportedEmployees.adapter = employeeDetailAdapter
    }
}