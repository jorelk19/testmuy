package com.example.testmuy.view.splashScreen

import android.os.Bundle
import android.os.Handler
import androidx.databinding.DataBindingUtil
import com.example.testmuy.BaseApp
import com.example.testmuy.R
import com.example.testmuy.databinding.ActivitySplashScreenBinding
import com.example.testmuy.utilities.BaseFragmentActivity
import com.example.testmuy.view.MainActivity

class SplashScreenActivity : BaseFragmentActivity() {

    private lateinit var activitySplashScreenBinding : ActivitySplashScreenBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activitySplashScreenBinding = DataBindingUtil.setContentView(this@SplashScreenActivity, R.layout.activity_splash_screen)

        Handler().postDelayed(Runnable {
            val appManager = (application as BaseApp).applicationManager
            appManager.activityManager.goTo(MainActivity::class.java)
            this.finish()
        }, 2000)
    }
}