package com.example.testmuy.view

import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.example.testmuy.BaseApp
import com.example.testmuy.R
import com.example.testmuy.databinding.ActivityMainBinding
import com.example.testmuy.utilities.BaseFragmentActivity

class MainActivity :  BaseFragmentActivity() {
    private lateinit var activityMainBinding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activityMainBinding = DataBindingUtil.setContentView(this@MainActivity, R.layout.activity_main)
        setSelectedBottomNavigationItem()
        setBottomNavigationListener()
    }

    private fun setSelectedBottomNavigationItem() {
        activityMainBinding.mainBottomNavigation.menu.getItem(0).isChecked = true
    }

    override fun onPostCreate(savedInstanceState: Bundle?) {
        super.onPostCreate(savedInstanceState)
        val appManager = (application as BaseApp).applicationManager
        appManager.activityManager.goToFragment(appManager.employeeFragment, R.id.fragment_container)
    }

    private fun setBottomNavigationListener() {
        activityMainBinding.mainBottomNavigation.setOnNavigationItemSelectedListener {
            when (it.itemId) {
                R.id.action_home -> {
                    if (!it.isChecked) {
                        val appManager = (application as BaseApp).applicationManager
                        appManager.activityManager.goToFragment(appManager.employeeFragment, R.id.fragment_container)
                        true
                    } else {
                        false
                    }
                }
                R.id.action_new_employees -> {
                    if (!it.isChecked) {
                        val appManager = (application as BaseApp).applicationManager
                        appManager.activityManager.goToFragment(appManager.storedEmployeeFragment, R.id.fragment_container)
                        true
                    } else {
                        false
                    }
                }
                else -> false
            }
        }
    }
}
