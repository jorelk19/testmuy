package com.example.testmuy.view.employee.adapters

import androidx.recyclerview.widget.RecyclerView
import com.example.testmuy.databinding.LayoutEmployeeItemBinding
import com.example.testmuy.model.Employee
import java.text.NumberFormat
import java.util.*
import kotlin.collections.ArrayList

class EmployeesHolder(private val layoutEmployeeItemBinding : LayoutEmployeeItemBinding, private var selectedEmployees : ArrayList<Employee>) : RecyclerView.ViewHolder(layoutEmployeeItemBinding.root) {

    fun bind(currentEmployee: Employee) {
        layoutEmployeeItemBinding.tvEmployeeName.text = currentEmployee.name
        layoutEmployeeItemBinding.tvPosition.text = currentEmployee.position
        layoutEmployeeItemBinding.tvEmployeeWage.text = NumberFormat.getCurrencyInstance(Locale.US).format(currentEmployee.wage).substringBefore(".")
        layoutEmployeeItemBinding.chkIsNewEmployee.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked){
                selectedEmployees.add(currentEmployee)
            }else{
                selectedEmployees.remove(currentEmployee)
            }
        }
    }
}