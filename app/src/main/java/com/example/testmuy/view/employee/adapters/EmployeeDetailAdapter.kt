package com.example.testmuy.view.employee.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.testmuy.R
import com.example.testmuy.databinding.LayoutEmployeeDetailItemBinding
import com.example.testmuy.model.Employee

class EmployeeDetailAdapter : RecyclerView.Adapter<EmployeeDetailHolder>() {
    private var employeeList: ArrayList<Employee> = arrayListOf()
    private lateinit var layoutEmployeeDetailItemBinding: LayoutEmployeeDetailItemBinding
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EmployeeDetailHolder {
        layoutEmployeeDetailItemBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.layout_employee_detail_item, parent, false)
        return EmployeeDetailHolder(layoutEmployeeDetailItemBinding)
    }

    override fun getItemCount(): Int {
        return employeeList.size
    }

    override fun onBindViewHolder(holder: EmployeeDetailHolder, position: Int) {
        holder.bind(employeeList[position])
    }

    fun setEmployees(addressList: ArrayList<Employee>) {
        employeeList.clear()
        employeeList.addAll(addressList)
        this.notifyDataSetChanged()
    }
}