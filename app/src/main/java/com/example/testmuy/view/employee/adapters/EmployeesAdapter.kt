package com.example.testmuy.view.employee.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.testmuy.R
import com.example.testmuy.databinding.LayoutEmployeeItemBinding
import com.example.testmuy.di.ApplicationManager
import com.example.testmuy.model.Employee
import com.example.testmuy.view.employee.EmployeeDetailFragment


class EmployeesAdapter(private val applicationManager: ApplicationManager) : RecyclerView.Adapter<EmployeesHolder>() {

    private var selectedEmployees = ArrayList<Employee>()
    private var employeeList: ArrayList<Employee> = arrayListOf()
    private lateinit var layoutEmployeeItemBinding: LayoutEmployeeItemBinding
    private lateinit var employeeHolder : EmployeesHolder

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EmployeesHolder {
        layoutEmployeeItemBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.layout_employee_item, parent, false)
        employeeHolder = EmployeesHolder(layoutEmployeeItemBinding, selectedEmployees).listen { pos, _ ->
            applicationManager.activityManager.goToFragment(EmployeeDetailFragment.newInstance(employeeList[pos], applicationManager), R.id.fragment_container)
        }
        return employeeHolder
    }

    override fun getItemCount(): Int {
        return employeeList.size
    }

    override fun onBindViewHolder(holder: EmployeesHolder, position: Int) {
        holder.bind(employeeList[position])
    }

    fun setEmployees(addressList: ArrayList<Employee>) {
        employeeList.clear()
        employeeList.addAll(addressList)
        this.notifyDataSetChanged()
    }

    private fun <T : RecyclerView.ViewHolder> T.listen(event: (position: Int, type: Int) -> Unit): T {
        itemView.setOnClickListener { event.invoke(adapterPosition, itemViewType) }
        return this
    }

    fun getEmployees(): ArrayList<Employee> {
        return selectedEmployees
    }
}