package com.example.testmuy.view.storedEmployee

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.testmuy.R
import com.example.testmuy.databinding.StoredEmployeeFragmentBinding
import com.example.testmuy.di.ApplicationManager
import com.example.testmuy.view.storedEmployee.adapters.StoredEmployeesAdapter
import com.example.testmuy.viewmodel.StoredEmployeeViewModel

class StoredEmployeeFragment(private val storedEmployeeViewModel: StoredEmployeeViewModel, private val applicationManager: ApplicationManager) : Fragment() {
    private lateinit var storedEmployeeFragmentBinding: StoredEmployeeFragmentBinding
    private var storedEmployeesAdapter: StoredEmployeesAdapter? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        storedEmployeeFragmentBinding = DataBindingUtil.inflate(inflater, R.layout.stored_employee_fragment, container, false)
        addSubscriptions()
        buildAdapter()
        setLayoutManager()
        setAdapters()
        loadNewEmployees()
        return storedEmployeeFragmentBinding.root
    }

    private fun addSubscriptions() {
        storedEmployeeViewModel.getLocalEmployeesLiveData().observe(viewLifecycleOwner, Observer { employees ->
            storedEmployeesAdapter?.setEmployees(employees)
        })
    }

    private fun loadNewEmployees() {
        storedEmployeeViewModel.getLocalEmployees()
    }

    private fun setLayoutManager() {
        storedEmployeeFragmentBinding.rvEmployee.layoutManager = LinearLayoutManager(applicationManager.currentContext)
    }

    private fun buildAdapter() {
        storedEmployeesAdapter = StoredEmployeesAdapter(applicationManager)
    }

    private fun setAdapters() {
        storedEmployeeFragmentBinding.rvEmployee.adapter = storedEmployeesAdapter
    }
}