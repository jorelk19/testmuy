package com.example.testmuy.view.employee

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.testmuy.R
import com.example.testmuy.databinding.EmployeeFragmentBinding
import com.example.testmuy.di.ApplicationManager
import com.example.testmuy.utilities.SnackFactory
import com.example.testmuy.view.employee.adapters.EmployeesAdapter
import com.example.testmuy.viewmodel.EmployeesViewModel

class EmployeeFragment(private val employeesViewModel: EmployeesViewModel, private val applicationManager: ApplicationManager, private val snackFactory: SnackFactory) : Fragment() {

    private lateinit var employeeFragmentBinding: EmployeeFragmentBinding
    private var employeesAdapter: EmployeesAdapter? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        super.onCreate(savedInstanceState)
        employeeFragmentBinding = DataBindingUtil.inflate(inflater, R.layout.employee_fragment, container, false)
        loadEmployees()
        buildAdapter()
        setAdapters()
        setLayoutManager()
        addSubscriptions()
        setListeners()
        return employeeFragmentBinding.root
    }

    private fun setListeners() {
        employeeFragmentBinding.chkSortEmployee.setOnCheckedChangeListener { _, isChecked ->
            employeesViewModel.setSortEmployees(isChecked)
        }

        employeeFragmentBinding.ivSearchEmployee.setOnClickListener {
            employeesViewModel.searchText(employeeFragmentBinding.etSearchEmployee.text.toString())
        }

        employeeFragmentBinding.btnSaveNewEmployee.setOnClickListener {
            employeesViewModel.saveEmployees(employeesAdapter?.getEmployees())
        }
    }

    private fun setLayoutManager() {
        employeeFragmentBinding.rvEmployee.layoutManager = LinearLayoutManager(applicationManager.currentContext)
    }

    private fun buildAdapter() {
        employeesAdapter = EmployeesAdapter(applicationManager)
    }

    private fun setAdapters() {
        employeeFragmentBinding.rvEmployee.adapter = employeesAdapter
    }

    private fun addSubscriptions() {
        employeesViewModel.getEmployeesLiveData().observe(viewLifecycleOwner, Observer { employees ->
            employeesAdapter?.setEmployees(employees)
        })

        employeesViewModel.getSortLiveData().observe(viewLifecycleOwner, Observer {
            employeeFragmentBinding.chkSortEmployee.isChecked = it
        })

        employeesViewModel.getSavedLiveData().observe(viewLifecycleOwner, Observer {
            if (it) {
                snackFactory.showSuccessMessage(applicationManager, R.id.coordinator_main_activity, applicationManager.activityManager.getString(R.string.saved_successful_massage))
            } else {
                snackFactory.showWarningMessage(applicationManager, R.id.coordinator_main_activity, applicationManager.activityManager.getString(R.string.saved_partial_massage))
            }
        })
    }

    private fun loadEmployees() {
        employeesViewModel.getEmployees()
    }

}