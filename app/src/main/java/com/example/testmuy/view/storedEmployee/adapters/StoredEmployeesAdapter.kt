package com.example.testmuy.view.storedEmployee.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.testmuy.R
import com.example.testmuy.databinding.LayoutEmployeeItemBinding
import com.example.testmuy.di.ApplicationManager
import com.example.testmuy.model.Employee
import com.example.testmuy.view.employee.EmployeeDetailFragment

class StoredEmployeesAdapter(private val applicationManager: ApplicationManager) : RecyclerView.Adapter<StoredEmployeesHolder>() {
    private var employeeList: ArrayList<Employee> = arrayListOf()
    private lateinit var layoutEmployeeItemBinding: LayoutEmployeeItemBinding
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): StoredEmployeesHolder {
        layoutEmployeeItemBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.layout_employee_item, parent, false)
        return StoredEmployeesHolder(layoutEmployeeItemBinding).listen { pos, _ ->
            applicationManager.activityManager.goToFragment(EmployeeDetailFragment.newInstance(employeeList[pos], applicationManager), R.id.fragment_container)
        }
    }

    override fun getItemCount(): Int {
        return employeeList.size
    }

    override fun onBindViewHolder(holder: StoredEmployeesHolder, position: Int) {
        holder.bind(employeeList[position])
    }

    fun setEmployees(addressList: ArrayList<Employee>) {
        employeeList.clear()
        employeeList.addAll(addressList)
        this.notifyDataSetChanged()
    }

    private fun <T : RecyclerView.ViewHolder> T.listen(event: (position: Int, type: Int) -> Unit): T {
        itemView.setOnClickListener { event.invoke(adapterPosition, itemViewType) }
        return this
    }
}