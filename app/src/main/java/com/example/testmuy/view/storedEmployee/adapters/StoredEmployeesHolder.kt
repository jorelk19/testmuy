package com.example.testmuy.view.storedEmployee.adapters

import androidx.recyclerview.widget.RecyclerView
import com.example.testmuy.databinding.LayoutEmployeeItemBinding
import com.example.testmuy.model.Employee
import java.text.NumberFormat
import java.util.*

class StoredEmployeesHolder(private val layoutEmployeeItemBinding : LayoutEmployeeItemBinding) : RecyclerView.ViewHolder(layoutEmployeeItemBinding.root) {
    fun bind(currentEmployee: Employee) {
        layoutEmployeeItemBinding.tvEmployeeName.text = currentEmployee.name
        layoutEmployeeItemBinding.tvPosition.text = currentEmployee.position
        layoutEmployeeItemBinding.tvEmployeeWage.text = NumberFormat.getCurrencyInstance(Locale.US).format(currentEmployee.wage).substringBefore(".")
        layoutEmployeeItemBinding.chkIsNewEmployee.isChecked = currentEmployee.isNewEmployee
        layoutEmployeeItemBinding.chkIsNewEmployee.isEnabled = false
    }
}