package com.example.testmuy.view.employee.adapters

import androidx.recyclerview.widget.RecyclerView
import com.example.testmuy.databinding.LayoutEmployeeDetailItemBinding
import com.example.testmuy.model.Employee

class EmployeeDetailHolder (private val layoutEmployeeDetailItemBinding : LayoutEmployeeDetailItemBinding) : RecyclerView.ViewHolder(layoutEmployeeDetailItemBinding.root) {
    fun bind(currentEmployee: Employee) {
        layoutEmployeeDetailItemBinding.tvEmployeeName.text = currentEmployee.name
        layoutEmployeeDetailItemBinding.tvPosition.text = currentEmployee.position
    }
}