package com.example.testmuy.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.testmuy.model.Employee
import com.example.testmuy.repository.RepositoryManager
import com.example.testmuy.utilities.StringUtils

class EmployeesViewModel(private val repositoryManager: RepositoryManager) : ViewModel() {
    var employees = MutableLiveData<ArrayList<Employee>>()
    var isSorted = MutableLiveData<Boolean>()
    var allSaved = MutableLiveData<Boolean>()

    fun getEmployeesLiveData(): LiveData<ArrayList<Employee>> {
        return employees
    }

    fun getSavedLiveData(): LiveData<Boolean> {
        return allSaved
    }

    fun getSortLiveData(): LiveData<Boolean> {
        return isSorted
    }

    fun getEmployees() {
        setEmployees(repositoryManager.getAllEmployees())
    }

    private fun setSavedState(isAllSaved: Boolean) {
        allSaved.value = isAllSaved
    }

    private fun setIsSorted(isChecked: Boolean) {
        isSorted.value = isChecked
    }

    fun setEmployees(currentEmployees: ArrayList<Employee>) {
        employees.value = currentEmployees
    }

    fun setSortEmployees(isChecked: Boolean) {
        setIsSorted(isChecked)
        if (isChecked) {
            employees.value?.let { list ->
                setEmployees(StringUtils.quickSort(list, 0, list.size - 1))
            }
        } else {
            getEmployees()
        }
    }

    fun searchText(text: String) {
        setIsSorted(false)
        if (text.isNullOrEmpty()) {
            getEmployees()
        } else {
            employees.value?.let { list ->
                setEmployees(StringUtils.searchText(list, text))
            }
        }
    }

    fun saveEmployees(newEmployees: ArrayList<Employee>?) {
        newEmployees?.let { list ->
            var cont = 0
            for (item in list) {
                if (repositoryManager.getLocalEmployee(item.id) == null) {
                    repositoryManager.createEmployee(item)
                    cont++
                }
            }
            setSavedState(cont == list.size)
            getEmployees()
        }
    }
}