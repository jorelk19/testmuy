package com.example.testmuy.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.testmuy.model.Employee
import com.example.testmuy.repository.RepositoryManager

class StoredEmployeeViewModel(private val repositoryManager: RepositoryManager) : ViewModel() {
    private val localEmployees = MutableLiveData<ArrayList<Employee>>()

    fun getLocalEmployees() {
        setLocalEmployees(repositoryManager.getAllLocalEmployees())
    }

    fun getLocalEmployeesLiveData() : LiveData<ArrayList<Employee>>{
        return localEmployees
    }

    private fun setLocalEmployees(employees: java.util.ArrayList<Employee>) {
        localEmployees.value = employees
    }


}