package com.example.testmuy

import android.app.Application
import com.example.testmuy.di.ApplicationManager

class BaseApp : Application() {
    var applicationManager = ApplicationManager()

    override fun onCreate() {
        super.onCreate()
        applicationManager.setContext(this)
    }
}