package com.example.testmuy.repository

import com.example.testmuy.model.Employee

class RepositoryManager(private val workerRemoteDataSource: EmployeeRemoteDataSource, private val workerLocalDataSource: EmployeeLocalDataSource){
    fun getAllEmployees() : ArrayList<Employee>{
        return workerRemoteDataSource.getAllEmployees().employees
    }

    fun getLocalEmployee(employeeId : Int) : Employee? {
        return workerLocalDataSource.runEmployeeQuery(employeeId)
    }

    fun getAllLocalEmployees() : ArrayList<Employee>{
        return workerLocalDataSource.getAllLocalEmployees()
    }

    fun createEmployee(employee: Employee) {
        val employeeData = "${employee.id}, '${employee.name}', '${employee.position}', ${employee.wage}, 1"
        workerLocalDataSource.createEmployee(employeeData)
    }
}