package com.example.testmuy.repository.database

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import java.io.File
import java.io.FileOutputStream
import java.io.InputStream
import java.io.OutputStream
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths

class DatabaseManager(private val context: Context) : SQLiteOpenHelper(context, DATABASE_NAME, null, DATABASE_VERSION) {

    companion object {

        private const val DATABASE_VERSION = 2
        private const val DATABASE_NAME = "TestMuyDB.db"
        private const val DATABASE_FOLDER = "/databases/"

        private var instance: DatabaseManager? = null
        fun getInstanceDB(context: Context) : DatabaseManager {
                if (instance == null) {
                    instance = DatabaseManager(context)
                    instance!!.databaseSetUp()
                }
                return instance!!

        }
    }

    private fun databaseSetUp() {
        val file = File(context.applicationInfo.dataDir + DATABASE_FOLDER + DATABASE_NAME)
        if(!file.exists()) {
            createDatabaseFolder()
            copyDatabase()
        }
    }

    // This method will called when database is updated
    fun setUpDatabase() {
        shutDownDatabase()
        updateCurrentDatabase()
    }

    // Shut down all cursors active
    private fun shutDownDatabase() = this.close()

    private fun updateCurrentDatabase() {
        instance = DatabaseManager(context)
    }

    // Get SQLiteDatabase readable mode
    fun getReadable(): SQLiteDatabase = this.readableDatabase

    fun getWriteable(): SQLiteDatabase = this.writableDatabase

    override fun onCreate(p0: SQLiteDatabase?) {}

    override fun onUpgrade(p0: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {}

    // Method to create folder if not exists
    fun createDatabaseFolder(): Boolean {
        val dbPath = context.applicationInfo.dataDir + DATABASE_FOLDER
        val dbFile: File = context.getDatabasePath(dbPath)
        dbFile.deleteRecursively()
        return File(dbPath).mkdirs()
    }

    private fun copyDatabase() {
        val dbFile = context.getDatabasePath(DATABASE_NAME)
        dbFile?.let {
            val inputS: InputStream = context.assets.open(DATABASE_NAME)
            val os: OutputStream = FileOutputStream(dbFile)
            val buffer = ByteArray(1024)
            while (inputS.read(buffer) > 0) {
                os.write(buffer)
            }
            os.flush()
            os.close()
            inputS.close()
        }
    }
}