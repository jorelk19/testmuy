package com.example.testmuy.repository.database

import android.database.Cursor
import com.example.testmuy.di.ApplicationManager
import io.reactivex.Observable
import java.io.BufferedReader
import kotlin.text.StringBuilder

class DatabaseUtils(private val applicationManager: ApplicationManager) {

    // Available Queries
    enum class Query {
        EMPLOYEE_QUERY,
        DELETE_EMPLOYEE_QUERY,
        CREATE_EMPLOYEE_QUERY
    }

    //Run specific query
    fun rawQuery(query: Query, whereBody: String? = null): Cursor {
        return when (query) {
            Query.EMPLOYEE_QUERY -> {
                readSqlFileAndReplace("queries/EmployeesQuery.sql", whereBody)
            }
            Query.DELETE_EMPLOYEE_QUERY -> {
                readSqlFileAndReplace("queries/DeleteEmployeesQuery.sql", whereBody)
            }
            Query.CREATE_EMPLOYEE_QUERY -> {
                readSqlFileAndReplace("queries/CreateEmployeesQuery.sql", whereBody)
            }
        }
    }

    fun execSQLQuery(nameFile: String, whereBody: String) {
        val inputStream = applicationManager.currentContext.resources.assets.open(nameFile)
        val reader = BufferedReader(inputStream.reader())
        val content = StringBuilder()
        reader.use { row ->
            var line = row.readLine()
            while (line != null) {
                content.append(line)
                line = row.readLine()
            }
        }
        DatabaseManager.getInstanceDB(applicationManager.currentContext).setUpDatabase()
        val db = DatabaseManager.getInstanceDB(applicationManager.currentContext).getWriteable()
        val finalQuery = whereBody?.let { return@let content.replace(":queryValues".toRegex(), " $whereBody ") } ?: content.replace(":queryValues".toRegex(), " ")
        db.execSQL(finalQuery)
    }

    // Read Sql file and replace it with 'where' expression
    private fun readSqlFileAndReplace(nameFile: String, whereBody: String?): Cursor {
        val inputStream = applicationManager.currentContext.resources.assets.open(nameFile)
        val reader = BufferedReader(inputStream.reader())
        val content = StringBuilder()
        reader.use { row ->
            var line = row.readLine()
            while (line != null) {
                content.append(line)
                line = row.readLine()
            }
        }
        DatabaseManager.getInstanceDB(applicationManager.currentContext).setUpDatabase()
        val db = DatabaseManager.getInstanceDB(applicationManager.currentContext).getReadable()
        val finalQuery = whereBody?.let { return@let content.replace(":queryValues".toRegex(), " $whereBody ") } ?: content.replace(":queryValues".toRegex(), " ")
        return db.rawQuery(finalQuery, null)
    }
}

