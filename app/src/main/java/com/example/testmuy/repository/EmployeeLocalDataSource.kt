package com.example.testmuy.repository

import android.util.Log
import com.example.testmuy.model.Employee
import com.example.testmuy.repository.database.DatabaseUtils
import com.example.testmuy.utilities.ActivityManager
import java.sql.SQLException
import java.util.*

class EmployeeLocalDataSource(private val databaseUtils: DatabaseUtils, private val activityManager: ActivityManager) {

    companion object {
        const val DEFAULT_WHERE_EMPLOYEE = " where employeeId = "
    }

    fun runEmployeeQuery(employeeId: Int? = null): Employee? {
        val cursor = databaseUtils.rawQuery(DatabaseUtils.Query.EMPLOYEE_QUERY, "$DEFAULT_WHERE_EMPLOYEE $employeeId")
        var employee: Employee? = null
        while (cursor.moveToNext()) {
            employee = Employee(
                id = cursor.getInt(cursor.getColumnIndex("employeeId")),
                name = cursor.getString(cursor.getColumnIndex("employeeName")),
                position = cursor.getString(cursor.getColumnIndex("employeePosition")),
                wage = cursor.getDouble(cursor.getColumnIndex("employeeWage")),
                isNewEmployee = cursor.getInt(cursor.getColumnIndex("employeeNew")) == 1
            )
        }
        cursor.close()
        return employee
    }

    fun getAllLocalEmployees(): ArrayList<Employee> {
        val employees: ArrayList<Employee> = arrayListOf()
        try {
            activityManager.showLoader()
            val cursor = databaseUtils.rawQuery(DatabaseUtils.Query.EMPLOYEE_QUERY)
            while (cursor.moveToNext()) {
                val employee = Employee(
                    id = cursor.getInt(cursor.getColumnIndex("employeeId")),
                    name = cursor.getString(cursor.getColumnIndex("employeeName")),
                    position = cursor.getString(cursor.getColumnIndex("employeePosition")),
                    wage = cursor.getDouble(cursor.getColumnIndex("employeeWage")),
                    isNewEmployee = cursor.getInt(cursor.getColumnIndex("employeeNew")) == 1
                )
                employees.add(employee)
            }
            cursor.close()

        } catch (exception: SQLException) {
            Log.d("EmployeeLocalDataSource", "getAllLocalEmployees: " + exception.message)
        } catch (exception: Exception) {
            Log.d("EmployeeLocalDataSource", "getAllLocalEmployees: " + exception.message)
        } finally {
            activityManager.hideLoader()
        }
        return employees
    }

    fun createEmployee(employeeData: String) {
        try {
            databaseUtils.execSQLQuery("queries/CreateEmployeesQuery.sql", employeeData)
        } catch (exception: SQLException) {
            Log.d("EmployeeLocalDataSource", "getAllLocalEmployees: " + exception.message)
        } catch (exception: Exception) {
            Log.d("EmployeeLocalDataSource", "getAllLocalEmployees: " + exception.message)
        } finally {
            activityManager.hideLoader()
        }
    }
}