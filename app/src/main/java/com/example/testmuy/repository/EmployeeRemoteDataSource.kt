package com.example.testmuy.repository

import android.util.Log
import com.example.testmuy.api.HrApi
import com.example.testmuy.model.CompanyResponse
import com.example.testmuy.utilities.ActivityManager
import kotlinx.coroutines.*
import retrofit2.HttpException
import java.util.concurrent.TimeoutException
import kotlin.coroutines.CoroutineContext

class EmployeeRemoteDataSource(
    private val hrApi: HrApi,
    private val activityManager: ActivityManager
) : CoroutineScope {
    private val job = Job()
    override val coroutineContext: CoroutineContext = job + Dispatchers.IO

    fun getAllEmployees(): CompanyResponse {
        var companyResponse = CompanyResponse()
        try {
            activityManager.showLoader()
            runBlocking(Dispatchers.IO) {
                val operationService = async { hrApi.getAllEmployees() }
                companyResponse = operationService.await()
            }
            activityManager.hideLoader()
        } catch (exception: HttpException) {
            Log.d("WorkerRemoteDataSource", "getAllWorkers: " + exception.message)
        } catch (exception: TimeoutException) {
            Log.d("WorkerRemoteDataSource", "getAllWorkers: " + exception.message)
        } catch (exception: Exception) {
            Log.d("WorkerRemoteDataSource", "getAllWorkers: " + exception.message)
        } finally {
            activityManager.hideLoader()
        }

        return companyResponse
    }
}