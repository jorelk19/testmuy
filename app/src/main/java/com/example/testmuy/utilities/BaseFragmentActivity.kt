package com.example.testmuy.utilities

import android.os.Bundle
import android.os.PersistableBundle
import androidx.fragment.app.FragmentActivity
import com.example.testmuy.BaseApp

open class BaseFragmentActivity : FragmentActivity() {

    override fun onCreate(savedInstanceState: Bundle?, persistentState: PersistableBundle?) {
        super.onCreate(savedInstanceState, persistentState)
        val appManager = (application as BaseApp).applicationManager
        appManager.activityManager.setCurrentActivity(this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val appManager = (application as BaseApp).applicationManager
        appManager.activityManager.setCurrentActivity(this)
    }
}