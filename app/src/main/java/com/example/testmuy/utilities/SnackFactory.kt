package com.example.testmuy.utilities

import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.core.content.ContextCompat
import androidx.fragment.app.FragmentActivity
import com.example.testmuy.R
import com.example.testmuy.di.ApplicationManager
import com.google.android.material.snackbar.Snackbar

object SnackFactory {

    fun showSuccessMessage(applicationManager: ApplicationManager, resource: Int, message: String) {
        Snackbar.make((applicationManager.activityManager.getCurrentActivity()).findViewById<CoordinatorLayout>(resource), message, Snackbar.LENGTH_SHORT)
            .setBackgroundTint(ContextCompat.getColor(applicationManager.currentContext, R.color.colorPrimary))
            .setTextColor(ContextCompat.getColor(applicationManager.currentContext, android.R.color.white))
            .setDuration(Snackbar.LENGTH_LONG)
            .show()
    }


    fun showWarningMessage(applicationManager: ApplicationManager, resource: Int, message: String) {
        Snackbar.make((applicationManager.activityManager.getCurrentActivity()).findViewById<CoordinatorLayout>(resource), message, Snackbar.LENGTH_SHORT)
            .setBackgroundTint(ContextCompat.getColor(applicationManager.currentContext , R.color.statusYellowLabel))
            .setTextColor(ContextCompat.getColor(applicationManager.currentContext , android.R.color.white))
            .setDuration(Snackbar.LENGTH_LONG)
            .show()
    }
}