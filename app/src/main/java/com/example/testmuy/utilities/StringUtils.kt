package com.example.testmuy.utilities

import com.example.testmuy.model.Employee

object StringUtils {
    fun sortStringArray(array: ArrayList<Employee>) : ArrayList<Employee> {
        for (i in 0 until array.size) {
            for (j in 1 until array.size - i) { //Apply the bubble Sort
                if (compareString(array[j - 1].name, array[j].name) == 1) { //Pass the two adjacent string for comparing
                    val temp = array[j - 1]
                    array[j - 1] = array[j]
                    array[j] = temp
                }
            }
        }
        return array
    }

    private fun compareString(first: String, second: String): Int {
        val len: Int = if (first.length >= second.length) //we need to take the smallest string length
            second.length else first.length
        for (i in 0..len) {
            if (first[i] > second[i]) //Suppose the first string letters is greater then return 1;
                return 1 else if (first[i] < second[i]) //if second string letter is greater then return -1;
                return -1
        }
        return 0 //if both the equal then return 0
    }

    fun quickSort(employeeList: ArrayList<Employee>, izq: Int, der: Int) : ArrayList<Employee>{
        val pivote = employeeList[izq]
        var i = izq
        var j = der
        var aux: Employee
        while (i < j) {
            while (employeeList[i].wage <= pivote.wage && i < j) i++
            while (employeeList[j].wage > pivote.wage) j--
            if (i < j) {
                aux = employeeList[i]
                employeeList[i] = employeeList[j]
                employeeList[j] = aux
            }
        }
        employeeList[izq] = employeeList[j]
        employeeList[j] = pivote
        if (izq < j - 1) quickSort(employeeList, izq, j - 1)
        if (j + 1 < der) quickSort(employeeList, j + 1, der)

        return employeeList
    }

    fun searchText(employeeList : ArrayList<Employee>, searchedText : String) : ArrayList<Employee> {
        val searchList = arrayListOf<Employee>()

        for(item in employeeList){
            if(item.name.contains(searchedText)){
                searchList.add(item)
            }
        }

        return searchList
    }
}