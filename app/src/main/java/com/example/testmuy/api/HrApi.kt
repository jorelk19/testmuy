package com.example.testmuy.api

import com.example.testmuy.model.CompanyResponse
import retrofit2.http.GET

interface HrApi {
    @GET("/sapardo10/content/master/RH.json")
    suspend fun getAllEmployees() : CompanyResponse
}