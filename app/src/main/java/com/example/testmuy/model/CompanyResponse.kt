package com.example.testmuy.model

import com.google.gson.annotations.SerializedName

data class CompanyResponse(
    @SerializedName("company_name")
    val companyName : String = "",
    val address : String = "",
    val employees : ArrayList<Employee> = arrayListOf()
)
