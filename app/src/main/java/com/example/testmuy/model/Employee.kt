package com.example.testmuy.model

data class Employee(
    val id : Int = 0,
    val name : String = "",
    val position : String = "",
    val wage : Double = 0.0,
    val employees: ArrayList<Employee> = arrayListOf(),
    val isNewEmployee: Boolean = false
)