package com.example.testmuy.di

import android.content.Context
import com.example.testmuy.BuildConfig
import com.example.testmuy.api.HrApi
import com.example.testmuy.repository.RepositoryManager
import com.example.testmuy.repository.EmployeeLocalDataSource
import com.example.testmuy.repository.EmployeeRemoteDataSource
import com.example.testmuy.repository.database.DatabaseUtils
import com.example.testmuy.utilities.ActivityManager
import com.example.testmuy.utilities.SnackFactory
import com.example.testmuy.view.employee.EmployeeFragment
import com.example.testmuy.view.storedEmployee.StoredEmployeeFragment
import com.example.testmuy.viewmodel.EmployeesViewModel
import com.example.testmuy.viewmodel.StoredEmployeeViewModel
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class ApplicationManager {
    private val retrofit = Retrofit.Builder()
        .baseUrl(BuildConfig.HR_SERVICE_URL)
        .addConverterFactory(GsonConverterFactory.create())
        .build()
        .create(HrApi::class.java)

    lateinit var currentContext : Context
    val activityManager = ActivityManager()
    private val databaseUtils = DatabaseUtils(this)
    private val workerRemoteDataSource = EmployeeRemoteDataSource(retrofit, activityManager)
    private val workerLocalDataSource = EmployeeLocalDataSource(databaseUtils, activityManager)
    private val repositoryManager = RepositoryManager(workerRemoteDataSource, workerLocalDataSource)
    private val employeesViewModel = EmployeesViewModel(repositoryManager)
    private val storedEmployeeViewModel = StoredEmployeeViewModel(repositoryManager)
    val employeeFragment = EmployeeFragment(employeesViewModel, this, SnackFactory)
    val storedEmployeeFragment = StoredEmployeeFragment(storedEmployeeViewModel, this)

    fun setContext(context : Context){
        currentContext = context
    }
}